#! /usr/bin/env python

import os
import sys
import time
import glob

CURSOR_UP_ONE = '\x1b[1A'
ERASE_LINE = '\x1b[2K'


def main(run = '-1'):
    if os.path.isfile('visuman.in'):
        fin = open('visuman.in')
        line = fin.readline()
        din = line.rstrip('\n')
    else:
        din = './out/'
    print 'Look files in the directory %s .' % din
    fpf = glob.glob(din+'run*_pf.root')
    fmcp = glob.glob(din+'run*_sel.root')
    if run == '-1':
        fpf.sort(key=lambda x: os.stat(os.path.join('./', x)).st_mtime)
        fmcp.sort(key=lambda x: os.stat(os.path.join('./', x)).st_mtime)
        print 'Use files:\n\t%s\n\t%s' % (fmcp[-1], fpf[-1])
        os.system('''root 'visu.C+("%s", "%s")' -l''' % (fmcp[-1], fpf[-1]))
    else:
        runl = len(run)
        run = '0'*(4-runl) + run
        for f in fpf:
            if run in f:
                pf = f
                break
        for f in fmcp:
            if run in f:
                mcp = f
                break
        print 'Use files:\n\t%s\n\t%s' % (mcp, pf)
        os.system('''root 'visu.C+("%s", "%s")' -l''' % (mcp, pf))


if __name__ == '__main__':
    if len(sys.argv) == 2:
        main(sys.argv[1])
    else:
        main()
