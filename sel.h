//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jan 20 18:33:54 2016 by ROOT version 5.32/04
// from TTree tree/tree
// found on file: Sr_source_darksheet_1by1_100us_1adc_MCP18_phos35_10000_20160118_001.root
//////////////////////////////////////////////////////////

#ifndef sel_h
#define sel_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>

// main
#include "TFileCollection.h"
#include "TEnv.h"
#include "TProof.h"
#include "TProofDebug.h"
#include "TString.h"
#include "TSocket.h"
#include "TSystem.h"
#include <TProofOutputFile.h>

// Header file for the classes stored in the TTree if any.
#include <TH2.h>

// #include "fast.h"

// Fixed size dimensions of array or collections stored in the TTree if any.

class sel : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain

   // Declaration of leaf types
   TH2F            *t1;

   // List of branches
   TBranch        *b_t1;   //!

   sel(TTree * /*tree*/ =0) : fChain(0) { }
   virtual ~sel() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(sel,0);
};

#endif

#ifdef sel_cxx
void sel::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   t1 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("t1", &t1, &b_t1);
}

Bool_t sel::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef sel_cxx
