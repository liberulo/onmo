#include <unistd.h>
#include "TROOT.h"
#include "TF1.h"
#include "TH1.h"
#include "TH2.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TStyle.h"


void analisys(TH2F * horiginal, int entry=0, TCanvas * c1=0){
    gStyle->SetOptFit(1101);//  Show fit results in a stat box.
    // gStyle->SetStatY(.95);
    // gStyle->SetStatX(.95);
    // gStyle->SetStatH(.25);
    // gStyle->SetStatW(.30);
    char c1title[250];
    // sprintf(c1title, "%s entry %d", fname, entry);
    // TCanvas * c1 = new TCanvas("c1", c1title, 1200, 700);
    // c1->Divide(3, 2);
    // TCanvas * c1 = new TCanvas("c1", c1title, 4*240, 2*240);
    // c1->Divide(4, 2);
    c1->cd(1);
    // TFile * f = new TFile("selout.root", "read");
    // TTree * t = (TTree *) f->Get("t");
    // t->Draw("cls.y:cls.x>>h()", "cls.hei>180", "goff");
    // TH2F * h = (TH2F *) gDirectory->Get("h");
    long nbinsx = horiginal->GetNbinsX();
    long nbinsy = horiginal->GetNbinsY();
    std::cout << "The number of bins along x-axis is " << nbinsx << ".\n";
    std::cout << "The number of bins along y-axis is " << nbinsy << ".\n";
    std::cout << "The total number of bins in the histogram is " << nbinsx*nbinsy << ".\n";
    float hmax = horiginal->GetMaximum();
    TH2F * h = (TH2F *) horiginal->Clone("h");
    h->SetTitle("Modified");
    TH1F * hbin = new TH1F("hbin", "hbin", 100, 0, hmax);
    for(long i=1; i<=nbinsx*nbinsy; i++){
        hbin->Fill(h->GetBinContent(i));
    }
    // TF1 * fbin = new TF1("fbin", "expo(0)");
    // TF1 * fbin = new TF1("fbin", "[0]*TMath::LogNormal(x,[1],[2],[3])");
    // fbin->SetParameters(100, 0.5, 0, 1);
    // hbin->Fit(fbin, "", "goff");
    // float tau = -2./fbin->GetParameter(1);
    float tau = hbin->GetBinCenter(hbin->GetMaximumBin()+6);
    std::cout << "tau = " << tau << "\n";
    float bc;// Bin Content.
    for(long j=1; j<=nbinsx*nbinsy; j++){
        bc = h->GetBinContent(j);
        // std::cout << j << " ";
        if(bc < tau){
            h->SetBinContent(j, 0);
            // std::cout << " 0" << h->GetBinContent(j) << "  ";
        }else{
            h->SetBinContent(j, bc-tau);
            // std::cout << bc-tau << " " << h->GetBinContent(j) << "  ";
        }
    }
    // std::cout << "\n";
    h->GetYaxis()->SetRangeUser(0, 1150);
    TH1F * hx = (TH1F *) h->ProjectionX("hpx");
    TH1F * hy = (TH1F *) h->ProjectionY("hpy");
    float hxmean = hx->GetMean();
    float hxrms = hx->GetRMS();
    float hymean = hy->GetMean();
    float hyrms = hy->GetRMS();
    TF1 * fx = new TF1("fx", "pol1(0)+gaus(2)");
    fx->SetParameters(10, 0, 1000, hxmean, hxrms);
    fx->SetParNames("c0", "c1", "A", "mean", "sigma");
    TF1 * fy = new TF1("fx", "pol1(0)+gaus(2)");
    fy->SetParameters(10, 0, 1000, hymean, hyrms);
    fy->SetParNames("c0", "c1", "A", "mean", "sigma");
    hx->Fit(fx, "", "goff");
    hy->Fit(fy, "", "goff");
    TF1 * fxpol = new TF1("fxpol", "pol1(0)");
    fxpol->SetParameters(fx->GetParameter(0), fx->GetParameter(1));
    fxpol->SetLineStyle(2);
    TF1 * fypol = new TF1("fypol", "pol1(0)");
    fypol->SetParameters(fy->GetParameter(0), fy->GetParameter(1));
    fypol->SetLineStyle(2);
    //  Peak slice analysis
    int mb = h->GetMaximumBin();
    int mbx, mby, mbz;
    h->GetBinXYZ(mb, mbx, mby, mbz);
    TH1F * slicex = (TH1F *) h->ProjectionX("slicex", mby-nbinsy/20, mby+nbinsy/20);
    TH1F * slicey = (TH1F *) h->ProjectionY("slicey", mbx-nbinsx/20, mbx+nbinsx/20);
    slicex->SetTitle("Slice X");
    slicey->SetTitle("Slice Y");
    float slicexmean = slicex->GetMean();
    float slicexrms = slicex->GetRMS();
    float sliceymean = slicey->GetMean();
    float sliceyrms = slicey->GetRMS();
    TF1 * fslicex = new TF1("fslicex", "pol1(0)+gaus(2)",
            .2*nbinsx*slicex->GetBinWidth(1), .8*nbinsx*slicex->GetBinWidth(1));
    fslicex->SetParameters(10, 0, 1000, slicexmean, slicexrms);
    fslicex->SetParNames("c0", "c1", "A", "mean", "sigma");
    TF1 * fslicey = new TF1("fslicey", "pol1(0)+gaus(2)",
            .2*nbinsy*slicey->GetBinWidth(1), .8*nbinsy*slicey->GetBinWidth(1));
    fslicey->SetParameters(10, 0, 1000, sliceymean, sliceyrms);
    fslicey->SetParNames("c0", "c1", "A", "mean", "sigma");
    slicex->Fit(fslicex, "R", "goff");
    slicey->Fit(fslicey, "R", "goff");
    //  The drawing section.
    c1->cd(1);
    horiginal->Draw("colz");
    c1->cd(2);
    hbin->Draw();
    c1->cd(3);
    h->Draw("colz");
    c1->cd(4);
    gStyle->SetOptStat(110001111);
    gStyle->SetStatX(1);
    hx->Draw();
    fxpol->Draw("same");
    fx->Draw("same");
    c1->cd(5);
    hy->Draw();
    fypol->Draw("same");
    fy->Draw("same");
    c1->cd(6);
    gPad->SetRightMargin(0.2);
    slicex->Draw();
    fslicex->Draw("same");
    c1->cd(7);
    gPad->SetRightMargin(0.2);
    slicey->Draw();
    fslicey->Draw("same");
    c1->cd();
    c1->Update();
    //  Output distribution characteristics.
    std::cout << "\nhx\n";
    std::cout << "Mean " << hxmean << "\n";
    std::cout << "RMS " << hxrms << "\n";
    std::cout << "Skewness " << hx->GetSkewness() << "\n";
    std::cout << "Kurtosis " << hx->GetKurtosis() << "\n";
    std::cout << "fx\n";
    std::cout << "Mean " << fx->GetParameter(3) << "\n";
    std::cout << "Sigma " << fx->GetParameter(4) << "\n";
    std::cout << "chi2 / ndf = " << fx->GetChisquare() << " / " << fx->GetNDF()
        << " = " << fx->GetChisquare()/fx->GetNDF() << "\n";
    std::cout << "\nhy\n";
    std::cout << "Mean " << hymean << "\n";
    std::cout << "RMS " << hyrms << "\n";
    std::cout << "Skewness " << hy->GetSkewness() << "\n";
    std::cout << "Kurtosis " << hy->GetKurtosis() << "\n";
    std::cout << "fy\n";
    std::cout << "Mean " << fy->GetParameter(3) << "\n";
    std::cout << "Sigma " << fy->GetParameter(4) << "\n";
    std::cout << "chi2 / ndf = " << fy->GetChisquare() << " / " << fy->GetNDF()
        << " = " << fy->GetChisquare()/fx->GetNDF() << "\n";
    /*
    if(isdelay == 1)
        sleep(2);
    if(issave == 1){
    std::cout << c1name << "\n";
        c1->SaveAs(c1name);
    }
    */
    return;
}


TH2F * getUV(string fname, int entry=0){
    std::cout << "Start getUV.\n";
    TFile * f = new TFile(fname.c_str(), "read");
    if(f->IsZombie())
        std::cout << "The file is a zombie.\n";
    TTree * t = (TTree *) f->Get("tree");
    std::cout << t << "\n";
    TH2F * h = 0;
    t->SetBranchAddress("t1", &h);
    t->GetEntry(entry);
    int nbinsx = h->GetNbinsX();
    int nbinsy = h->GetNbinsY();
    for(int i=1; i<=nbinsx*30; i++){
        h->SetBinContent(i, 0);
    }
    // h->Rebin2D(16, 16);
    std::cout << "End getUV.\n";
    return h;
}


void demo(string fmcp="out/run0002_online_sel.root",
        string fpos="out/run0002_pf.root",
        int entry=0, string c1name="nonomo.png"){
    // gROOT->ProcessLine(".x tunePalette.C");
    /*
    TFile * f = new TFile("selout.root", "read");
    TTree * t = (TTree *) f->Get("t");
    t->Draw("cls.y:cls.x>>h()", "cls.hei>180", "goff");
    TH2F * h = (TH2F *) gDirectory->Get("h");
    */
    TH2F * h = 0;
    h = (TH2F *) getUV(fmcp, entry);
    TCanvas * c1 = new TCanvas("c1", c1name.c_str(), 5*200, 3*200);
    c1->Divide(5, 3);
    analisys(h, entry, c1);
}
