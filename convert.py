#! /usr/bin/env python

import os
import sys
import time
import glob


def mainall():
    items = glob.glob('/Volumes/HD-LCU3/mubpm/201602/mcp/raw/run'+
            '[0-9]'*4+'_'+'[0-9]'*4+'*2by2*.pcoraw')
    for fin in items:
        fout = fin.replace('/raw/', '/converted/').replace('.pcoraw', '.root')
        if os.path.isfile(fout):
            continue
        os.system('''root 'pco2root2x2.C+("%s", "%s")' -b -q''' % (fin, fout))


def mainpatt(patt):
    items = glob.glob(patt)
    files = []
    print 'These files will be converted:'
    for fin in items:
        fout = fin.replace('.pcoraw', '.root')
        if os.path.isfile(fout):
            continue
        files.append([fin, fout])
        print fin
    print 'The total number of files is {}.'.format(len(files))
    ans = raw_input('Would you like to convert them? (yes/no)  ')
    if ans not in ['yes', 'Yes']:
        return
    for f in files:
        os.system('''root 'pco2root2x2.C+("%s", "%s")' -b -q''' % (f[0], f[1]))


def main(run, subrun):
    run = '0'*(4-len(run)) + run
    subrun = '0'*(4-len(subrun)) + subrun
    items = glob.glob('/Volumes/HD-LCU3/mubpm/201602/mcp/raw/run'+
            '[0-9]'*4+'_'+'[0-9]'*4+'*2by2*.pcoraw')
    rsr = 'run'+run+'_'+subrun
    fin = False
    for item in items:
        if rsr in item:
            fin = item
    if not fin:
        return
    fout = fin.replace('/raw/', '/converted/').replace('.pcoraw', '.root')
    print '\t{}\n\t{}'.format(fin, fout)
    os.system('''root 'pco2root2x2.C+("%s", "%s")' -b -q''' % (fin, fout))


if __name__ == '__main__':
    if len(sys.argv) == 3:
        if sys.argv[1] == '-patt':
            mainpatt(sys.argv[2])
        main(sys.argv[1], sys.argv[2])
    if len(sys.argv) == 2:
        if sys.argv[1] == '-all':
            mainall()
