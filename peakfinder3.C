//2016.2.18 by hani + bongho
/// digitizer data to root file & peak finding code 
/// macro_code
#include <fstream>
#include <iostream>
#include <sstream>
#include "Riostream.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TF1.h"
#include "TH1.h"
#include "TH2.h"
#include <vector>


void peakfinder3(string finname){
    using namespace std;
    // TFile *f = new TFile("Acqiris.root");
    TFile *f = new TFile(finname.c_str());
    TTree *t = (TTree*) f->Get("digitizerTree");
    //   t->Print();
    ofstream outfile("peak.txt");

    //variables
    Int_t N = 100; //# of trigger
    Int_t samp_num = 200; //# of samples
    Float_t threshold = 0.1;
    Int_t tot_num = samp_num*N;
    Int_t width = 30;  //width of cosmic ray
    Int_t width2; //width of MCP signal
    Float_t vol1;
    Float_t vol2;
    Float_t vol3;
    Float_t vol4;
    vector< float > ypeaks;
    vector< float > ypeaks2;
    vector< float > ypeaks3;
    vector< float > ypeaks4;
    Float_t time;
    Int_t triggertime;   

    //set branch address   
    t->SetBranchAddress("trig_time", &triggertime);
    t->SetBranchAddress("timestamp", &time);
    t->SetBranchAddress("v_ch1", &vol1);
    t->SetBranchAddress("v_ch2", &vol2);
    t->SetBranchAddress("v_ch3", &vol3);
    t->SetBranchAddress("v_ch4", &vol4);
    TH2F *h1 = new TH2F("h1", "ch1", tot_num, 0, tot_num, 1000, 0, 1);
    TH2F *h2 = new TH2F("h2", "ch2", tot_num, 0, tot_num, 1000, 0, 1);
    TH2F *h3 = new TH2F("h3", "ch3", tot_num, 0, tot_num, 1000, 0, 1);
    TH2F *h4 = new TH2F("h4", "ch4", tot_num, 0, tot_num, 1000, -0.01, 0.05);
    TH1F *h5 = new TH1F("h5", "PHD ch1", 1000, 0, 1);
    TH1F *h6 = new TH1F("h6", "PHD ch2", 1000, 0, 1);   
    TH1F *h7 = new TH1F("h7", "PHD ch3", 1000, 0, 1);
    TH1F *h8 = new TH1F("h8", "PHD ch4", 1000, 0, 1);
    //read entries

    Int_t nn = (Int_t)t->GetEntries();
    for(Int_t i=0; i<nn; i++){
        t->GetEntry(i);
        ypeaks.push_back(-1*vol1);
        ypeaks2.push_back(-1*vol2);
        ypeaks3.push_back(-1*vol3);
        //ypeaks4.push_back(vol4);
        h1->Fill(i, ypeaks.at(i));
        h2->Fill(i, ypeaks2.at(i));
        h3->Fill(i, ypeaks3.at(i));
        // h4->Fill(i,ypeaks4.at(i));
    }

    //find peak code
    // TCanvas *c1 = new TCanvas("c1", "positron counter", 1200, 800);
    // c1->Divide(3, 2);
    //ch1
    Int_t npeaks = 0;
    Float_t mxm;
    Int_t bin, j;

    for(Int_t i=0; i<tot_num; i++){
        if(ypeaks.at(i)>threshold && ypeaks.at(i+width)<threshold){ //give threshold
        mxm = ypeaks[i];                                      //find maximum
        for(j=0; j<width; j++){
            if(ypeaks.at(i+j)>=mxm){
                mxm = ypeaks.at(i+j);
                bin = i+j;
            }
        }   
        h5->Fill(mxm);
        outfile << "bin : " << bin << " yp : " << mxm << endl;
        i += width;
        npeaks++;
        }
    }
    cout << npeaks << endl;
    // c1->cd(1);
    // h5->Draw();

    //ch2
    outfile << "ch2 start" << endl;

    npeaks = 0;
    for(Int_t i=0; i<tot_num; i++){
        if(ypeaks2.at(i)>threshold && ypeaks2.at(i+width)<threshold){
        mxm = ypeaks2.at(i);
        for(j=0; j<width; j++){
            if(ypeaks2.at(i+j)>=mxm){
                mxm = ypeaks2.at(i+j);
                bin = i+j;
            }
        }
        h6->Fill(mxm);
        outfile << "bin : " << bin << " yp : " << mxm << endl;
        i += width;
        npeaks++;
        }
    }
    cout << npeaks << endl;
    // c1->cd(2);
    // h6->Draw();
    //ch3

    outfile << "ch3 start" << endl;

    npeaks = 0;
    for(Int_t i=0; i<tot_num; i++){
        if(ypeaks3.at(i)>threshold && ypeaks3.at(i+width)<threshold){
            mxm = ypeaks3.at(i);
            for(j=0; j<width; j++){
                if(ypeaks3.at(i+j)>=mxm){
                    mxm = ypeaks3.at(i+j);
                    bin = i+j;
                }
            }
            h7->Fill(mxm);
            outfile << "bin : " << bin << " yp : " << mxm << endl;
            i += width;
            npeaks++;
        }
    }
    cout << npeaks << endl;
    // c1->cd(3); 
    // h7->Draw();

    // c1->cd(4);
    // h1->Draw();
    // c1->cd(5);
    // h2->Draw();
    // c1->cd(6);
    // h3->Draw();

    TFile * Fout = new TFile("pf.root", "recreate");
    h1->Write("h1");
    h2->Write("h2");
    h3->Write("h3");
    h5->Write("h5");
    h6->Write("h6");
    h7->Write("h7");
    Fout->Close();

    outfile.close();
}
