#! /usr/bin/env python

import os
import sys
import time
import glob

CURSOR_UP_ONE = '\x1b[1A'
ERASE_LINE = '\x1b[2K'


def uselog(logname, files):
    runs = [[f, f.split('run')[-1][:9]] for f in files]
    islog = os.path.isfile(logname)
    if islog:
        flog = open(logname)
        lines = flog.readlines()
        rlog = [line.rstrip('\n') for line in lines]
    else:
        rlog = []
    newfiles = []
    for run in runs:
        if not run[1] in rlog:
            newfiles.append(run[0])
    if len(newfiles) > 0:
        flag = True
    else:
        flag = False
    return flag, newfiles


def isnewmcpfile(dirs):
    fmcp = glob.glob(dirs['mcpraw']+'*run[0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]*online*.pcoraw')
    fmcp += glob.glob(dirs['mcpraw']+'*online*run[0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]*.pcoraw')
    return uselog('watchdogmcp.log', fmcp)


def isnewposfile(dirs):
    fpos = glob.glob(dirs['posraw']+'*run[0-9][0-9][0-9][0-9]_[0-9][0-9][0-9][0-9]*.root')
    return uselog('watchdogpos.log', fpos)


def getdirs(wdin):
    f = open(wdin)
    lines = [x.rstrip('\n').split(' ') for x in f.readlines()]
    return dict((x[0], x[1]) for x in lines)


def isnewfile(dirmcp, dirpos):
    '''
    Goes through the files in input directories. Selects files
    contained 'online' and 'run[][][][]' in their names.
    Makes pairs of files with the same run number. Checks if
    they already have been analysed then exuledes them from
    the list of pairs. Returns the list of new file pairs and
    a flag.
    '''
    fmcp = glob.glob(dirmcp+'*run[0-9]*online*.pcoraw')
    fmcp += glob.glob(dirmcp+'*online*run[0-9]*.pcoraw')
    fpos = glob.glob(dirpos+'*run[0-9]*.root')
    # print 'Have found %d MCP online run files.' % len(fmcp)
    # print 'Have found %d positron counter run files.' % len(fpos)
    rpos = [[f, f.split('run')[-1][:4]] for f in fpos]
    # print rmcp, rpos
    islog = os.path.isfile('watchdog.log')
    if islog:
        flog = open('watchdog.log')
        lines = flog.readlines()
        rlog = [line.rstrip('\n') for line in lines]
    else:
        lines = []
        rlog = []
    for rm in rmcp:
        if rm[1] in rlog:
            rm.append(False)
        else:
            rm.append(True)
    for rp in rpos:
        if rp[1] in rlog:
            rp.append(False)
        else:
            rp.append(True)
    pairs = []
    for rm in rmcp:
        if not rm[2]:
            continue
        if 'tmp' in rm[0].lower():
            continue
        for rp in rpos:
            if not rp[2]:
                continue
            if rp[1] == rm[1]:
                pairs.append([rm[0], rp[0]])
                rp[2] = False
    if islog:
        flog.close()
    if len(pairs) > 0:
        flag = True
    else:
        # print 'pairs', pairs
        flag = False
    return flag, pairs


def mainmcp(wdin='watchdog.in'):
    dirs = getdirs(wdin)
    print 'The MCP raw data directory:\t%s' % dirs['mcpraw']
    print 'The MCP converted data directory:\t%s' % dirs['mcpconv']
    print 'The MCP online data directory:\t%s' % dirs['mcponline']
    tdelay = 10
    totdelay = 0
    while True:
        isNewFile, fmcp = isnewmcpfile(dirs)
        if isNewFile:
            for f in fmcp :
                fmcpraw = f
                fmcproo = dirs['mcpconv']+f.split('/')[-1].split('.pcora')[0]+'.root'
                fmcpsel = dirs['mcponline']+f.split('/')[-1].split('.pcora')[0]+'_sel.root'
                print 'Launch the convertor.'
                if '2by2' in fmcpraw.lower() or '2x2' in fmcpraw.lower():
                    print '2x2 pixel mode.'
                    os.system('''root 'pco2root2x2.C+("%s", "%s")' -b -q''' % (fmcpraw, fmcproo))
                elif '1by1' in fmcpraw.lower() or '1x1' in fmcpraw.lower():
                    print '1x1 pixel mode.'
                    os.system('''~/ccd_study/ccd_file_conv/pco %s %s''' % (fmcpraw, fmcproo))
                else:
                    print "The pixel mode is not specified. Please add '1by1' or '2by2' to the name of the file."
                    print "This file now will be skipped."
                    continue
                print 'Launch the proof manager.'
                os.system('''root 'prooman.C+("%s")' -b -q''' % fmcproo)
                os.system('''cp -rf selout.root %s''' % fmcpsel)
                flog = open('watchdogmcp.log', 'a')
                flog.write('%s\n' % (f.split('run')[1][:9]))
                flog.close()
                continue
            totdelay = 0
        if totdelay > 0:
            print CURSOR_UP_ONE+ERASE_LINE+'Waiting %d seconds.' % totdelay
        else:
            print 'Waiting %d seconds.' % totdelay
        totdelay += tdelay
        time.sleep(tdelay)


def mainpos(wdin='watchdog.in'):
    dirs = getdirs(wdin)
    print 'The positron counter raw data directory:\t%s' % dirs['posraw']
    print 'The positron counter online data directory:\t%s' % dirs['posonline']
    tdelay = 10
    totdelay = 0
    while True:
        isNewFile, fpos = isnewposfile(dirs)
        if isNewFile:
            for f in fpos :
                fposin = f
                print 'Launch peak finding.'
                os.system('''root 'peakfinder5.C+("%s")' -b -q''' % fposin)
                fpospf = dirs['posonline']+fposin.split('/')[-1].split('.roo')[0]+'_pf.root'
                os.system('''cp -rf pf.root %s''' % fpospf)
                fpostxt = fpospf.split('.roo')[0]+'.txt'
                os.system('''cp -rf peak.txt %s''' % fpostxt)
                flog = open('watchdogpos.log', 'a')
                flog.write('%s\n' % (f.split('run')[1][:9]))
                flog.close()
                continue
            totdelay = 0
        if totdelay > 0:
            print CURSOR_UP_ONE+ERASE_LINE+'Waiting %d seconds.' % totdelay
        else:
            print 'Waiting %d seconds.' % totdelay
        totdelay += tdelay
        time.sleep(tdelay)


def main(dirmcp='mcp/', dirpos='positron/', dirout='out/'):
    '''
    Looks for new files, converts and analyse them. At the end
    produce files which are suitable for a visualisation tool.
    '''
    for x in [dirmcp, dirpos, dirout]:
        if x[-1] != '/':
            x += '/'
    print 'The input MCP file directory is %s.' % dirmcp
    print 'The input positron counter file directory is %s.' % dirpos
    print 'The output file directory is %s.' % dirout
    tdelay = 10
    totdelay = 0
    while True:
        isNewFile, pairs = isnewfile(dirmcp, dirpos)
        if isNewFile:
            for pair in pairs:
                fmcpraw = pair[0]
                fposin = pair[1]
                fmcproo = dirout+fmcpraw.split('/')[-1].split('.pcora')[0]+'.root'
                print 'Launch the convertor.'
                if '2by2' in fmcpraw.lower() or '2x2' in fmcpraw.lower():
                    print '2x2 pixel mode.'
                    os.system('''root 'pco2root2x2.C+("%s", "%s")' -b -q''' % (fmcpraw, fmcproo))
                elif '1by1' in fmcpraw.lower() or '1x1' in fmcpraw.lower():
                    print '1x1 pixel mode.'
                    os.system('''~/ccd_study/ccd_file_conv/pco %s %s''' % (fmcpraw, fmcproo))
                else:
                    print "The pixel mode is not specified. Please add '1by1' or '2by2' to the name of the file."
                    print "This pair will be skipped."
                    continue
                print 'Launch the proof manager.'
                os.system('''root 'prooman.C+("%s")' -b -q''' % fmcproo)
                fmcpsel = fmcproo.split('.roo')[0]+'_sel.root'
                os.system('''cp -rf selout.root %s''' % fmcpsel)
                print 'Launch peak finding.'
                os.system('''root 'peakfinder5.C+("%s")' -b -q''' % fposin)
                fpospf = dirout+fposin.split('/')[-1].split('.roo')[0]+'_pf.root'
                os.system('''cp -rf pf.root %s''' % fpospf)
                fpostxt = fpospf.split('.roo')[0]+'.txt'
                os.system('''cp -rf peak.txt %s''' % fpostxt)
                flog = open('watchdog.log', 'a')
                flog.write('%s\n' % (fmcpraw.split('run')[1][:4]))
                flog.close()
                continue
            totdelay = 0
        if totdelay > 0:
            print CURSOR_UP_ONE+ERASE_LINE+'Waiting %d seconds.' % totdelay
        else:
            print 'Waiting %d seconds.' % totdelay
        totdelay += tdelay
        time.sleep(tdelay)


def usage():
        print 'Usage:'
        print '\t%s [mode] [file with directories]' % sys.argv[0]
        print '\t%s mcp' % sys.argv[0]
        print '\t%s pos' % sys.argv[0]
        print '\t%s mcp watchdog.in' % sys.argv[0]
        print '\t%s pos watchdog.in' % sys.argv[0]


if __name__ == '__main__':
    if len(sys.argv) == 2:
        if sys.argv[1].lower() == 'mcp':
            mainmcp()
        elif sys.argv[1].lower() == 'pos':
            mainpos()
        else:
            usage()
    elif len(sys.argv) == 3:
        if sys.argv[1].lower() == 'mcp':
            mainmcp(sys.argv[2])
        elif sys.argv[1].lower() == 'pos':
            mainpos(sys.argv[2])
        else:
            usage()
    else:
        usage()
