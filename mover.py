#! /usr/bin/env python

import os
import sys
import time
import glob

CURSOR_UP_ONE = '\x1b[1A'
ERASE_LINE = '\x1b[2K'


def move(fpathin):
    if '_tmp_' in fpathin:
        fpathout = fpathin.replace('_tmp_', '_')
    elif '_tmp.' in fpathin:
        fpathout = fpathin.replace('_tmp.', '.')
    elif '/tmp_' in fpathin:
        fpathout = fpathin.replace('/tmp_', '/')
    elif 'tmp_' == fpathin[:4]:
        fpathout = fpathin.replace('tmp_', '')
    else:
        print "Didn't regognise a 'tmp' pattern.\n\t%s" % fpathin
        return 0
    print 'The output file name is\n\t%s' % fpathout
    fsize = 0
    flag = -1
    totwai = 0
    while True:
        finfo = os.stat(fpathin)
        if finfo.st_size == 0 or finfo.st_size > fsize:
            if flag != -1:
                print CURSOR_UP_ONE+ERASE_LINE+'The file size is changing. (%d sec)' % totwai
            else:
                print 'The file size is changing. (%d sec)' % totwai
            fsize = finfo.st_size
            flag = 0
            time.sleep(1)
            totwai += 1
        else:
            if flag == 2:
                print 'The file size is stable.'
                break
            else:
                print 'The file size looks stable, wait 1 second more.'
                flag += 1
                time.sleep(1)
                totwai = 0
    os.system('''mv -f %s %s''' % (fpathin, fpathout))
    print 'Have moved the file.'


def main(dpath = './mcp/'):
    if dpath[-1] != '/':
        dpath += '/'
    print 'The working directory is {0}.'.format(dpath)
    noth = 0
    while True:
        files = glob.glob(dpath+'*tmp*.pcoraw')
        for f in files:
            move(f)
            noth = 0
        if noth == 0:
            print 'Nothing happens for %d seconds.' % noth
        else:
            print CURSOR_UP_ONE+ERASE_LINE+'Nothing happens for %d seconds.' % noth
        time.sleep(5)
        noth += 5


if __name__ == '__main__':
    if len(sys.argv) == 2:
        main(sys.argv[1])
    else:
        main()
